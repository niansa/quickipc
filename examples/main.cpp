#include "QIPC/ipc.hpp"

#include <iostream>



void child(QIPC& ipc) {
    ipc.send("Hello world!");
    ipc.send_raw(size_t(1234567890));
}

int main() {
    QIPC ipc;
    ipc.create();

    if (fork() == 0) {
        child(ipc);
        exit(EXIT_SUCCESS);
    }

    std::cout << ipc.recv() << std::endl;
    std::cout << ipc.recv_raw<size_t>() << std::endl;
}
